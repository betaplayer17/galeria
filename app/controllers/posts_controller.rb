class PostsController < ApplicationController
    def post_params
        params.require(:post).permit(:description,:image)
    end
    
  def new
      @post = Post.new
  end

  def index
      @posts = Post.all
  end
    
  def show
      @post = Post.find(params[:id])
  end
    
    def upvote
        @post = Post.find(params[:id])
        @post.upvote_from current_user
        redirect_to post_path(@post)
    end
    
    def downvote
        @post = Post.find(params[:id])
        @post.downvote_from current_user
        redirect_to post_path(@post)
    end
    
    def create
        @post= Post.new(post_params)
        if @post.save
            flash[:success] = "Su foto ha sido publicada"
            redirect_to post_path(@post)
        else
            flash[:error] = @post.errors.full_messages
            redirect_to new_post_path
        end
    end
    
def update
    respond_to do |format|
        @post = Post.find(params[:id])
        if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end
def edit
        @post = Post.find(params[:id])
    end

def destroy
    @post = Post.find(params[:id])
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def permit_post
        params.require(:post).permit(:image,:description)
    end
end
